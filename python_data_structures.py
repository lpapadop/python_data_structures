import sys
import resource
from array import array
from collections import deque
from collections import OrderedDict
from collections import defaultdict
import numpy as np

NUM_ELEMENTS = 100000
ARRAY = True
LIST = False
DEQUE = False
NP_ARRAY = False
DICT = False 		# does not preserve the order by which the elements are inserted
ORDERED_DICT = False 	# preserves the order by which the elements are inserted 
DEFAULT_DICT = False    # if a key does not exist, it is created with default value (zero)
#--------------------#
EXP1 = True
EXP2 = False

#################################################################
# auxiliary functions for profiling:
def np_array_remove(arr, end):
	if (end == 'right'):
		arr = arr[:-1]
	else:
		arr = arr[1:]
	return arr

def dict_insert(dict, index, value):
	dict[index] = value

# data structure definition:
if ARRAY: 
	a = array('I')
if LIST:
	a = []
if DEQUE: 
	a = deque('')
if NP_ARRAY:
	a = np.array([], dtype=np.int)
if DICT:
	a = {}
if ORDERED_DICT:
	a = OrderedDict()
if DEFAULT_DICT:
	a = defaultdict(int)

# operations:
if EXP1:
	# insert at the end:
	for i in range(NUM_ELEMENTS):
		if NP_ARRAY:
			a = np.append(a, i)
		if DICT or ORDERED_DICT or DEFAULT_DICT:
			dict_insert(a, i, i)
		else:
			a.append(i)
	# print data structure size: 
	if NP_ARRAY:
		print 'numpy array size (B): ', sys.getsizeof(a) + a.nbytes
	else:
		print 'data structure size (B): ', sys.getsizeof(a)
	# remove from the end:
	for i in range(NUM_ELEMENTS):
		if NP_ARRAY:
			a = np_array_remove(a, 'right')
		if DICT or ORDERED_DICT or DEFAULT_DICT:
			a.pop(NUM_ELEMENTS - 1 - i)
		else:
			a.pop() 	
if EXP2:
	#insert at the beginning:
	for i in range(NUM_ELEMENTS):
		if DEQUE:
			a.appendleft(i)
		if NP_ARRAY:
			a = np.insert(a, 0, i)
		if DICT or ORDERED_DICT or DEFAULT_DICT:
			dict_insert(a, NUM_ELEMENTS - 1 - i, i)
		else:
			a.insert(0, i)
	#remove from the beginning:
	for i in range(NUM_ELEMENTS): 
		if DEQUE:
			a.popleft()
		if NP_ARRAY:
			a = np_array_remove(a, 'left')
		if DICT or ORDERED_DICT or DEFAULT_DICT:
			a.pop(i)
		else:
			a.pop(0)

# total occupied memory:
print 'total required memory (KB): ', resource.getrusage(resource.RUSAGE_SELF).ru_maxrss # KB
